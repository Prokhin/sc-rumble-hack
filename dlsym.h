/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#include <dlfcn.h>

extern "C" void *real_dlsym(void *handle, const char *name);
