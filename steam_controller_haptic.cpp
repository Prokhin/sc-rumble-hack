/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#include "steam_controller_haptic.h"
#include "steam_controllers_thread.h"
#include "logger.h"
#include <inttypes.h>

void SteamControllerHapticEffect::update(const SteamControllerHapticEffectData &effectData)
{
    m_isEmpty = false;
    /* SDL_HapticEffect has max magnitude of 32767, XInput expects 65535 max, so multiply */
    m_leftSpeed = effectData.leftSpeed * 2;
    m_rightSpeed = effectData.rightSpeed * 2;
    m_length = effectData.length;
    m_delay = effectData.delay;
}

void SteamControllerHapticEffect::clear()
{
    m_isEmpty = true;
    m_isActive = false;
    m_leftSpeed = 0;
    m_rightSpeed = 0;
    m_length = 0;
    m_delay = 0;
}

_SDL_Haptic::_SDL_Haptic(SteamControllersThread *thread, ControllerHandle_t handle):
    m_thread(thread),
    m_handle(handle)
{
    m_thread->registerHaptic(this);
}

_SDL_Haptic::~_SDL_Haptic()
{
    m_thread->unregisterHaptic(this);
}

int _SDL_Haptic::addEffect(const SteamControllerHapticEffectData &effectData)
{
    TRACE("addEffect(leftSpeed=%u,rightSpeed=%u,length=%u,delay=%u)",
          effectData.leftSpeed, effectData.rightSpeed, effectData.length, effectData.delay);
    std::lock_guard<std::mutex> guard(m_mutex);

    for (int i = 0; i < MAX_EFFECTS; ++i) {
        auto &s = m_effects[i];
        if (s.isEmpty()) {
            s.update(effectData);
            m_thread->onHapticEffectUpdated(this, s);
            return i;
        }
    }

    return -1; // no free slots
}

bool _SDL_Haptic::updateEffect(int slotIndex, const SteamControllerHapticEffectData &effectData)
{
    TRACE("updateEffect(slotIndex=%i, leftSpeed=%u,rightSpeed=%u,length=%u,delay=%u)",
          slotIndex, effectData.leftSpeed, effectData.rightSpeed, effectData.length, effectData.delay);
    std::lock_guard<std::mutex> guard(m_mutex);
    assert(slotIndex >= 0 && slotIndex < MAX_EFFECTS);
    auto &s = m_effects[slotIndex];
    if (s.isEmpty())
        return false;
    s.update(effectData);
    m_thread->onHapticEffectUpdated(this, s);
    return true;
}

bool _SDL_Haptic::clearEffect(int slotIndex)
{
    TRACE("clearEffect(slotIndex=%i)", slotIndex);
    std::lock_guard<std::mutex> guard(m_mutex);
    assert(slotIndex >= 0 && slotIndex < MAX_EFFECTS);
    auto &s = m_effects[slotIndex];
    if (s.isEmpty())
        return false;
    s.clear();
    m_thread->onHapticEffectUpdated(this, s);
    return true;
}

bool _SDL_Haptic::triggerVibration(int slotIndex, Uint32 iterations)
{
    TRACE("triggerVibration(slotIndex=%i,iterations=%u)", slotIndex, iterations);
    std::lock_guard<std::mutex> guard(m_mutex);
    assert(slotIndex >= 0 && slotIndex < MAX_EFFECTS);
    auto &s = m_effects[slotIndex];
    if (s.isEmpty())
        return false;
    m_thread->onHapticEffectStarted(this, s, iterations);
    return true;
}

bool _SDL_Haptic::stopVibration(int slotIndex)
{
    TRACE("stopVibration(slotIndex=%i)", slotIndex);
    std::lock_guard<std::mutex> guard(m_mutex);
    assert(slotIndex >= 0 && slotIndex < MAX_EFFECTS);
    auto &s = m_effects[slotIndex];
    if (s.isEmpty())
        return false;
    m_thread->onHapticEffectStopped(this, s);
    return true;
}

void _SDL_Haptic::stopAllVibration()
{
    TRACE("stopAllVibration()");
    m_thread->onAllHapticEffectsStopped(this);
}

int _SDL_Haptic::addRumbleEffect()
{
    TRACE("addRumbleEffect()");
    if (m_rumbleId == -1)
        m_rumbleId = addEffect({0, 0});
    return m_rumbleId;
}

bool _SDL_Haptic::updateRumbleEffect(const SteamControllerHapticEffectData &effectData)
{
    TRACE("updateRumbleEffect(leftSpeed=%u,rightSpeed=%u,length=%u,delay=%u)",
          effectData.leftSpeed, effectData.rightSpeed, effectData.length, effectData.delay);
    if (m_rumbleId == -1)
        return false;
    return updateEffect(m_rumbleId, effectData);
}

void _SDL_Haptic::clearRumbleEffect()
{
    TRACE("clearRumbleEffect()");
    if (m_rumbleId == -1)
        return;
    clearEffect(m_rumbleId);
    m_rumbleId = -1;
}

bool _SDL_Haptic::triggerRumble()
{
    TRACE("triggerRumble()");
    if (m_rumbleId == -1)
        return false;
    return triggerVibration(m_rumbleId);
}

bool _SDL_Haptic::stopRumble()
{
    TRACE("stopRumble()");
    if (m_rumbleId == -1)
        return false;
    return stopVibration(m_rumbleId);
}
