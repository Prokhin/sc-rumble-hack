/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#pragma once
#include <SDL2/SDL_haptic.h>
#include <mutex>
#include "isteamcontroller.h"

class SteamControllersThread;

constexpr size_t MAX_EFFECTS = 16;

struct SteamControllerHapticEffectData
{
    Uint16 leftSpeed;
    Uint16 rightSpeed;
    Uint32 length = SDL_HAPTIC_INFINITY;
    Uint32 delay = 0;
};

struct SteamControllerHapticEffect
{
public:
    bool isEmpty() const { return m_isEmpty; }
    void update(const SteamControllerHapticEffectData &effectData);
    void clear();
private:
    bool m_isEmpty = true;
    Uint16 m_leftSpeed = 0;
    Uint16 m_rightSpeed = 0;
    Uint32 m_length = 0;
    Uint32 m_delay = 0;
private:
    // the data is managed by SteamControllersThread
    friend class SteamControllersThread;
    bool m_isActive = false;
    Uint32 m_resetTicks = 0;
    Uint32 m_startTicks = 0;
};

struct _SDL_Haptic
{
public:
    int m_ref = 0;

    _SDL_Haptic(SteamControllersThread *thread, ControllerHandle_t handle);
    ~_SDL_Haptic();

    ControllerHandle_t handle() const { return m_handle; }

    int addEffect(const SteamControllerHapticEffectData &effectData);
    bool updateEffect(int slotIndex, const SteamControllerHapticEffectData &effectData);
    bool clearEffect(int slotIndex);
    bool triggerVibration(int slotIndex, Uint32 iterations = 1);
    bool stopVibration(int slotIndex);
    void stopAllVibration();

    int addRumbleEffect();
    bool updateRumbleEffect(const SteamControllerHapticEffectData &effectData);
    bool triggerRumble();
    bool stopRumble();
    void clearRumbleEffect();
private:
    SteamControllersThread *m_thread;
    int m_rumbleId = -1;
private:
    friend class SteamControllersThread;
    std::mutex m_mutex;
    ControllerHandle_t m_handle;
    std::array<SteamControllerHapticEffect, MAX_EFFECTS> m_effects;
};
