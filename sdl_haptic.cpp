/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#include "dlsym.h"
#include "logger.h"
#include "steam_client.h"
#include "steam_controller_haptic.h"
#include "steam_controllers_thread.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_haptic.h>

#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <map>
#include <set>
#include <thread>

static int m_numberOfInitializations = 0;
static SteamControllersThread *m_thread = nullptr;
static SteamClientWrapper *m_steamClient = nullptr;
static std::map<ControllerHandle_t, SDL_Haptic*> m_haptics;

static void ensureLog()
{
    static bool initialized = false;
    if (initialized)
        return;

    if (auto logFile = std::getenv("SC_RUMBLE_HACK_LOG_FILE"))
        loguru::add_file(logFile, loguru::Append, loguru::Verbosity_MAX);
    initialized = true;
}

bool SDL_HapticInit()
{
    ensureLog();
    TRACE("SDL_HapticInit()");
    
    m_steamClient = new SteamClientWrapper;
    if (m_steamClient->controller() == nullptr) {
        TRACE("Controller has not been created!");
        delete m_steamClient; m_steamClient = nullptr;
        return false;
    }

    m_thread = new SteamControllersThread(m_steamClient->controller());
    return true;
}

void SDL_HapticQuit()
{
    TRACE("SDL_HapticQuit()");
    delete m_steamClient; m_steamClient = nullptr;
    delete m_thread; m_thread = nullptr;
}

int SDL_Init(Uint32 flags)
{
    ensureLog();
    TRACE("SDL_Init(flags=%#010x)", flags);

    typedef int (*Func)(Uint32);
    auto origFunc = reinterpret_cast<Func>(real_dlsym(RTLD_NEXT, "SDL_Init"));
    if (origFunc == nullptr) {
        LOG_F(ERROR, "The original SDL_Init has not been found");
        return -1;
    }

    if (flags & SDL_INIT_HAPTIC) {
        if (SDL_InitSubSystem(SDL_INIT_HAPTIC) < 0)
            return -1;
        // don't init the haptic subsystem from SDL itself, we completely replace it
        flags &= ~SDL_INIT_HAPTIC;
    }

    return (*origFunc)(flags);
}

int SDL_InitSubSystem(Uint32 flags)
{
    ensureLog();
    TRACE("SDL_QuitSubSystem(flags=%#010x)", flags);

    if (flags == SDL_INIT_HAPTIC) {
        if (m_numberOfInitializations == 0) {
            if (!SDL_HapticInit())
                return -1;
        }
        ++m_numberOfInitializations;
        return 0;
    } else {
        typedef int (Func)(Uint32);
        auto origFunc = reinterpret_cast<Func*>(real_dlsym(RTLD_NEXT, "SDL_InitSubSystem"));
        if (origFunc != nullptr)
            return (*origFunc)(flags);
        return -1;
    }
}

void SDL_QuitSubSystem(Uint32 flags)
{
    TRACE("SDL_QuitSubSystem(flags=%#010x)", flags);
    if (flags == SDL_INIT_HAPTIC) {
        if (m_numberOfInitializations > 0) {
            --m_numberOfInitializations;
            if (m_numberOfInitializations == 0)
                SDL_HapticQuit();
        }
    } else {
        typedef int (Func)(Uint32);
        auto origFunc = reinterpret_cast<Func*>(real_dlsym(RTLD_NEXT, "SDL_QuitSubSystem"));
        if (origFunc != nullptr)
            (*origFunc)(flags);
    }
}

void SDL_Quit()
{
    TRACE("SDL_Quit()");
    SDL_QuitSubSystem(SDL_INIT_HAPTIC);

    typedef int (Func)();
    auto origFunc = reinterpret_cast<Func*>(real_dlsym(RTLD_NEXT, "SDL_Quit"));
    if (origFunc != nullptr)
        (*origFunc)();
}

Uint32 SDL_WasInit(Uint32 flags)
{
    TRACE("SDL_WasInit(flags=%#010x)", flags);
    Uint32 r = 0;

    typedef Uint32 (Func)(Uint32);
    auto origFunc = reinterpret_cast<Func*>(real_dlsym(RTLD_NEXT, "SDL_WasInit"));
    if (origFunc != nullptr)
        r = (*origFunc)(flags);

    if ((flags == 0 || flags == SDL_INIT_HAPTIC) && m_numberOfInitializations > 0)
        r |= SDL_INIT_HAPTIC;

    return r;
}

static bool isValid(SDL_Haptic *haptic)
{
    return std::find_if(m_haptics.begin(), m_haptics.end(), [=](auto &&p) {
        return p.second == haptic;
    }) != m_haptics.end();
}

static bool isMultiJoystickSupportedEnabled()
{
    static bool isEnabled = [] {
        const char *c = std::getenv("SC_RUMBLE_HACK_MULTIJOYSTICK_SUPPORT");
        bool e = c != nullptr && (strcmp(c, "1") == 0 || strcmp(c, "true") == 0);
        if (e)
            LOG_F(INFO, "Multi-device support is enabled");
        else
            LOG_F(INFO, "Multi-device support is disabled");
        return e;
    }();
    return isEnabled;
}

static int findJoystickIndex(SDL_Joystick *joystick)
{
    SDL_JoystickGUID device_guid = SDL_JoystickGetGUID(joystick);
    for (int i = 0; i < SDL_NumJoysticks(); ++i) {
        auto other_guid = SDL_JoystickGetDeviceGUID(i);
        if (memcmp(device_guid.data, other_guid.data, sizeof(other_guid.data)) == 0) {
            TRACE("Index for joystick %p is %i (instanceId=%i)", joystick, i, SDL_JoystickInstanceID(joystick));
            return i;
        }
    }
    TRACE("Index for joystick %p is not found", joystick);
    return -1;
}

static ControllerHandle_t findHandle(int device_index)
{
    auto controller = m_steamClient->controller();
    controller->RunFrame();
    auto handle = controller->GetControllerForGamepadIndex(device_index);
    TRACE("Joystick %i has SCAPI handle %llu", device_index, handle);
    return handle;
}

int SDL_JoystickIsHaptic(SDL_Joystick *joystick)
{
    TRACE("SDL_JoystickIsHaptic(joystick=%p)", joystick);
    if (isMultiJoystickSupportedEnabled()) {
        int device_index = findJoystickIndex(joystick);
        if (device_index == -1)
            return SDL_FALSE;
        return findHandle(device_index) ? SDL_TRUE : SDL_FALSE;
    } else {
        return SDL_TRUE;
    }
}

SDL_Haptic *SDL_HapticOpen(int device_index)
{
    TRACE("SDL_HapticOpen(device_index=%i)", device_index);
    auto handle = findHandle(isMultiJoystickSupportedEnabled() ? device_index : 0);
    if (!handle)
        return nullptr;

    auto hapticItr = m_haptics.find(handle);
    SDL_Haptic *haptic = nullptr;
    if (hapticItr == m_haptics.end()) {
        haptic = new SDL_Haptic(m_thread, handle);
        m_haptics.insert({handle, haptic});
    } else {
        haptic = hapticItr->second;
    }

    ++haptic->m_ref;
    return haptic;
}

int SDL_HapticOpened(int device_index)
{
    TRACE("SDL_HapticOpened(device_index=%i)", device_index);
    auto handle = findHandle(isMultiJoystickSupportedEnabled() ? device_index : 0);
    if (!handle)
        return SDL_FALSE;
    return m_haptics.find(handle) != m_haptics.end() ? SDL_TRUE : SDL_FALSE;
}

SDL_Haptic *SDL_HapticOpenFromJoystick(SDL_Joystick *joystick)
{
    TRACE("SDL_HapticOpenFromJoystick(joystick=%p)", joystick);
    if (isMultiJoystickSupportedEnabled()) {
        return SDL_HapticOpen(findJoystickIndex(joystick));
    } else {
        return SDL_HapticOpen(0);
    }
}

SDL_Haptic *SDL_HapticOpenFromMouse()
{
    TRACE("SDL_HapticOpenFromMouse()");
    return nullptr;
}

void SDL_HapticClose(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticClose(haptic=%p)", haptic);
    if (!isValid(haptic))
        return;

    --haptic->m_ref;
    if (haptic->m_ref == 0) {
        m_haptics.erase(m_haptics.find(haptic->handle()));
        delete haptic;
    }
}

int SDL_HapticIndex(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticIndex(haptic=%p)", haptic);
    if (!isValid(haptic))
        return false;

    auto controller = m_steamClient->controller();
    return controller->GetGamepadIndexForController(haptic->handle());
}

const char *SDL_HapticName(int device_index)
{
    TRACE("SDL_HapticName(device_index=%i)", device_index);
    return "Microsoft X-Box 360 pad";
}

int SDL_HapticNumAxes(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticNumAxes(haptic=%p)", haptic);
    return 2;
}

int SDL_HapticPause(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticPause(haptic=%p)", haptic);
    return SDL_Unsupported();
}

int SDL_HapticUnpause(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticUnpause(haptic=%p)", haptic);
    return SDL_Unsupported();
}

unsigned int SDL_HapticQuery(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticQuery(haptic=%p)", haptic);
    return isValid(haptic) ? SDL_HAPTIC_LEFTRIGHT | SDL_HAPTIC_CONSTANT : 0;
}

static int isEffectSlotValid(int effect)
{
    return effect >= 0 && effect < MAX_EFFECTS;
}

static bool isEffectSupported(SDL_HapticEffect *effect)
{
    if ((effect->type & SDL_HAPTIC_LEFTRIGHT) != 0)
        return true;

    if ((effect->type & SDL_HAPTIC_CONSTANT) != 0)
        return true;

    return false;
}

static SteamControllerHapticEffectData convertEffect(SDL_HapticEffect *effect)
{
    if (effect->type == SDL_HAPTIC_LEFTRIGHT) {
        TRACE("SDL_HAPTIC_LEFTRIGHT(large_magnitude=%u,small_magnitude=%u,length=%u)",
              effect->leftright.large_magnitude, effect->leftright.small_magnitude, effect->leftright.length);
        return { effect->leftright.large_magnitude, effect->leftright.small_magnitude, effect->leftright.length };
    } else if (effect->type == SDL_HAPTIC_CONSTANT) {
        TRACE("SDL_HAPTIC_CONSTANT(level=%i,ength=%u,delay=%u)",
              effect->constant.level, effect->constant.length, effect->constant.delay);

        auto convLevel = [](Sint16 l) -> Uint16 {
            return l < 0 ? 0 : Uint16(l);
        };
        return { convLevel(effect->constant.level), convLevel(effect->constant.level), effect->constant.length, effect->constant.delay };
    }

    assert(false && "Unsupported effect");
    return {};
}

int SDL_HapticNewEffect(SDL_Haptic *haptic, SDL_HapticEffect *effect)
{
    TRACE("SDL_HapticNewEffect(haptic=%p,effect=%p,type=%i)", haptic, effect, effect->type);
    if (!isValid(haptic) || !isEffectSupported(effect))
        return -1;

    int slotIndex = haptic->addEffect(convertEffect(effect));
    if (slotIndex >= 0)
        return slotIndex;

    return SDL_SetError("Haptic: Device has no free space left.");
}

void SDL_HapticDestroyEffect(SDL_Haptic *haptic, int effect)
{
    TRACE("SDL_HapticDestroyEffect(haptic=%p,effect=%i)", haptic, effect);
    if (!isValid(haptic) || !isEffectSlotValid(effect))
        return;

    haptic->stopVibration(effect);
    haptic->clearEffect(effect);
}

int SDL_HapticUpdateEffect(SDL_Haptic *haptic, int effect, SDL_HapticEffect *data)
{
    TRACE("SDL_HapticUpdateEffect(haptic=%p,effect=%i,data=%p)", haptic, effect, data);
    if (!isValid(haptic) || !isEffectSupported(data) || !isEffectSlotValid(effect))
        return -1;

    if (!haptic->updateEffect(effect, convertEffect(data)))
        return -1;

    return 0;
}

int SDL_HapticEffectSupported(SDL_Haptic *haptic, SDL_HapticEffect *effect)
{
    TRACE("SDL_HapticEffectSupported(haptic=%p,effect=%p)", haptic, effect);
    if (!isValid(haptic))
        return -1;

    return isEffectSupported(effect) ? SDL_TRUE : SDL_FALSE;
}

int SDL_HapticGetEffectStatus(SDL_Haptic *haptic, int effect)
{
    TRACE("SDL_HapticGetEffectStatus(haptic=%p,effect=%i)", haptic, effect);
    return SDL_Unsupported();
}

int SDL_HapticRunEffect(SDL_Haptic *haptic, int effect, Uint32 iterations)
{
    TRACE("SDL_HapticRunEffect(haptic=%p,effect=%i,iterations=%i)", haptic, effect, iterations);
    if (!isValid(haptic) || !isEffectSlotValid(effect))
        return -1;

    if (!haptic->triggerVibration(effect, iterations))
        return -1;

    return 0;
}

int SDL_HapticStopEffect(SDL_Haptic *haptic, int effect)
{
    TRACE("SDL_HapticStopEffect(haptic=%p,effect=%i)", haptic, effect);
    if (!isValid(haptic) || !isEffectSlotValid(effect))
        return -1;

    if (!haptic->stopVibration(effect))
        return -1;

    return 0;
}

int SDL_HapticStopAll(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticStopAll(haptic=%p)", haptic);
    if (!isValid(haptic))
        return -1;

    haptic->stopAllVibration();
    return 0;
}

int SDL_HapticNumEffects(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticNumEffects(haptic=%p)", haptic);
    return MAX_EFFECTS;
}

int SDL_HapticNumEffectsPlaying(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticNumEffectsPlaying(haptic=%p)", haptic);
    return MAX_EFFECTS;
}

int SDL_HapticSetGain(SDL_Haptic *haptic, int gain)
{
    TRACE("SDL_HapticSetGain(haptic=%p,gain=%i)", haptic, gain);
    return SDL_Unsupported();
}

int SDL_HapticSetAutocenter(SDL_Haptic *haptic, int autocenter)
{
    TRACE("SDL_HapticSetAutocenter(haptic=%p,autocenter=%i)", haptic, autocenter);
    return SDL_Unsupported();
}

int SDL_HapticRumbleInit(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticNumEffectsPlaying(haptic=%p)", haptic);
    if (!isValid(haptic))
        return -1;

    if (haptic->addRumbleEffect() >= 0)
        return 0;

    return -1;
}

int SDL_HapticRumblePlay(SDL_Haptic *haptic, float strength, Uint32 length)
{
    TRACE("SDL_HapticRumblePlay(haptic=%p, strength=%g, length=%i)", haptic, strength, length);
    if (!isValid(haptic))
        return -1;

    /* Clamp strength. */
    if (strength > 1.0f) {
        strength = 1.0f;
    } else if (strength < 0.0f) {
        strength = 0.0f;
    }
    Uint16 magnitude = (Uint16)(65535.0f*strength);

    if (!haptic->updateRumbleEffect({ magnitude, magnitude, length }))
        return -1;

    if (haptic->triggerRumble())
        return -1;

    return 0;
}

int SDL_HapticRumbleStop(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticRumbleStop(haptic=%p)", haptic);
    if (!isValid(haptic))
        return -1;

    if (!haptic->stopRumble())
        return -1;

    return 0;
}

int SDL_HapticRumbleSupported(SDL_Haptic *haptic)
{
    TRACE("SDL_HapticRumbleSupported(haptic=%p)", haptic);
    if (!isValid(haptic))
        return -1;
    return SDL_TRUE;
}
