cmake_minimum_required(VERSION 3.6)
project(sc_rumble_hack VERSION 0.1 LANGUAGES CXX)
set (CMAKE_CXX_STANDARD 14)

list(APPEND CMAKE_MODULE_PATH
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake
)

find_package(SDL2 REQUIRED)
find_package(Steamworks REQUIRED)

if (CMAKE_SIZEOF_VOID_P EQUAL 8)
    add_definitions(-DARCH=64)
elseif (CMAKE_SIZEOF_VOID_P EQUAL 4)
    add_definitions(-DARCH=32)
endif()

add_library(
    sc_rumble_hack SHARED
    sdl_haptic.cpp
    dlsym.cpp
    logger.cpp
    steam_client.cpp
    steam_controller_haptic.cpp
    steam_controllers_thread.cpp
)
set_source_files_properties(dlsym.cpp PROPERTIES COMPILE_FLAGS " -Os -foptimize-sibling-calls ")
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    set_property(SOURCE dlsym.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -fno-cxx-exceptions ")
endif()
target_include_directories(sc_rumble_hack PRIVATE
    ${CMAKE_SOURCE_DIR}/3rdparty
    ${SDL2_INCLUDE_DIRS}
    ${STEAMWORKS_INCLUDE_DIR}
)
target_link_libraries(sc_rumble_hack
    ${SDL2_LIBRARIES}
    -static-libgcc
    -static-libstdc++
)
target_compile_definitions(sc_rumble_hack PRIVATE
    -DBUILD_SDL
)
