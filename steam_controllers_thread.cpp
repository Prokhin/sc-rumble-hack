/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#include "steam_controllers_thread.h"
#include "logger.h"

#include <algorithm>
#include <chrono>
#include <stdlib.h>
#include <stdio.h>

#include <SDL2/SDL.h>
#include <steam_api.h>

SteamControllersThread::SteamControllersThread(ISteamController *controller):
    m_isStopped(false), m_controller(controller)
{
    m_thread = new std::thread([this] {
        TRACE("Steam controllers thread has been started");

        while (!m_isStopped) {
            bool wasPaused = false;
            {
                std::unique_lock<std::mutex> guard(m_mutex);
                if (tick())
                    m_cond.wait_for(guard, std::chrono::milliseconds(50));
                else
                    m_cond.wait(guard);
            }
        }
    });
}

SteamControllersThread::~SteamControllersThread()
{
    {
        std::unique_lock<std::mutex> guard(m_mutex);
        m_isStopped = true;
        m_cond.notify_one();
    }

    TRACE("Waiting on steam controllers thread to be finished...");
    m_thread->join();
    delete m_thread;
    TRACE("Steam controllers thread has been stopped");
}

void SteamControllersThread::registerHaptic(SDL_Haptic *haptic)
{
    TRACE("registerHaptic(haptic=%p)", haptic);
    std::unique_lock<std::mutex> guard(m_mutex);
    m_haptics.push_back(haptic);
    TRACE("New haptic have been registered");
}

void SteamControllersThread::unregisterHaptic(SDL_Haptic *haptic)
{
    TRACE("unregisterHaptic(haptic=%p)", haptic);
    std::unique_lock<std::mutex> guard(m_mutex);
    auto itr = std::find(m_haptics.begin(), m_haptics.end(), haptic);
    if (itr != m_haptics.end()) {
        m_haptics.erase(itr);
        TRACE("Haptic have been unregistered");
    }
}

void SteamControllersThread::onHapticEffectStarted(SDL_Haptic *haptic, SteamControllerHapticEffect &effect, Uint32 iterations)
{
    TRACE("onHapticEffectStarted(haptic=%p,iterations=%u)", haptic, iterations);
    effect.m_isActive = true;

    if (iterations <= 0)
        iterations = 1;

    Uint32 currentTicks = SDL_GetTicks();
    effect.m_startTicks = currentTicks + effect.m_delay;
    if (effect.m_length == SDL_HAPTIC_INFINITY)
        effect.m_resetTicks = SDL_HAPTIC_INFINITY;
    else
        effect.m_resetTicks = currentTicks + (effect.m_length * iterations);

    m_cond.notify_one();
}

void SteamControllersThread::onHapticEffectUpdated(SDL_Haptic *haptic, SteamControllerHapticEffect &effect)
{
    TRACE("onHapticEffectUpdated(haptic=%p)", haptic);
    m_cond.notify_one();
}

void SteamControllersThread::onHapticEffectStopped(SDL_Haptic *haptic, SteamControllerHapticEffect &effect)
{
    TRACE("onHapticEffectStopped(haptic=%p)", haptic);
    effect.m_isActive = false;
    m_cond.notify_one();
}

void SteamControllersThread::onAllHapticEffectsStopped(SDL_Haptic *haptic)
{
    TRACE("onAllHapticEffectsStopped(haptic=%p)", haptic);
    for (auto &e : haptic->m_effects)
        e.m_isActive = false;
    m_cond.notify_one();
}

bool SteamControllersThread::tick()
{
    if (m_controller)
        m_controller->RunFrame();

    bool isAtLeastOneEffectActive = false;
    for (auto haptic : m_haptics) {
        std::lock_guard<std::mutex> guard(haptic->m_mutex);
        int leftSpeed = 0;
        int rightSpeed = 0;

        for (auto &effect : haptic->m_effects) {
            if (!effect.m_isEmpty && effect.m_isActive) {
                Uint32 currentTicks = SDL_GetTicks();
                bool isStarted = SDL_TICKS_PASSED(currentTicks, effect.m_startTicks);
                if (isStarted) {
                    bool isFinished = effect.m_resetTicks != SDL_HAPTIC_INFINITY && SDL_TICKS_PASSED(currentTicks, effect.m_resetTicks);
                    if (isFinished) {
                        effect.m_isActive = false;
                    } else if (effect.m_leftSpeed != 0 || effect.m_rightSpeed != 0) {
                        leftSpeed += effect.m_leftSpeed;
                        rightSpeed += effect.m_rightSpeed;
                        isAtLeastOneEffectActive = true;
                    }
                }
            }
        }

        leftSpeed = std::min(leftSpeed, 65535);
        rightSpeed = std::min(rightSpeed, 65535);

        updateVibrationState(haptic, Uint16(leftSpeed), Uint16(rightSpeed));
    }
    return isAtLeastOneEffectActive;
}

void SteamControllersThread::updateVibrationState(SDL_Haptic *haptic, Uint16 leftSpeed, Uint16 rightSpeed)
{
    TRACE("updateVibrationState(haptic=%p,leftSpeed=%u,rightSpeed=%u)", haptic, leftSpeed, rightSpeed);
    if (m_controller)
        m_controller->TriggerVibration(haptic->handle(), leftSpeed, rightSpeed);
}
