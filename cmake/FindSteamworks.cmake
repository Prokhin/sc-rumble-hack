set(STEAMWORKS_POSSIBLE_PATHS
    ${STEAMWORKS_SDK}
    $ENV{STEAMWORKS_SDK}
)

find_path(STEAMWORKS_INCLUDE_DIR 
    NAMES steam_api.h
    PATH_SUFFIXES "public/steam"
    PATHS ${STEAMWORKS_POSSIBLE_PATHS}
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Steamworks DEFAULT_MSG STEAMWORKS_INCLUDE_DIR)
