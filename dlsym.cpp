/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

// Unfortunately, just using LD_PREPEND to load the library is not good enough:
// dlsym() that is used heavily by wine would still return functions from the real SDL.
// We have to override dlsym as well.
// However, steam's overlay is up to the same trickery we are and also overrides dlsym().
// We have to take special care to return to the overlay real functions and not functions
// the overlay tries to override.

#include "dlsym.h"
#include <cstdlib>
#include <link.h>
#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_haptic.h>

typedef void *(*DLSym)(void *, const char *);

// We override dlsym() but have to get a real dlsym() from libdl.so.
// Unfortunately, there is no way to do that without relying on some
// private methods.
extern "C" void *_dl_sym(void *, const char *, void *);
static DLSym get_real_dlsym()
{
    static DLSym dlsym = reinterpret_cast<DLSym>(_dl_sym(RTLD_NEXT, "dlsym", reinterpret_cast<void*>(get_real_dlsym)));
    return dlsym;
}

static bool endsWith(const std::string &fullString, const std::string &ending)
{
    if (fullString.length() < ending.length())
        return false;

    return fullString.compare(fullString.length() - ending.length(), ending.length(), ending) == 0;
}

static std::string getLibraryNameFromHandle(void *handle)
{
    if (handle == RTLD_DEFAULT || handle == RTLD_NEXT)
        return {};

    const struct link_map* link_map = 0;
    if (dlinfo(handle, RTLD_DI_LINKMAP, &link_map) == 0) {
        std::string filePath(link_map->l_name);
        std::string fileName = filePath.substr(filePath.find_last_of("/\\") + 1);
        return fileName;
    }
    return {};
}

extern "C" __attribute__ ((noinline)) void *real_dlsym(void *handle, const char *name)
{
    return get_real_dlsym()(handle, name);
}

extern "C" void *__attribute__ ((noinline)) dlsym_impl(void *handle, const char *name)
{
#define OVERRIDE(func) \
    if (strcmp( name, #func ) == 0) \
        return reinterpret_cast<void*>( func );

    if (handle == RTLD_DEFAULT || handle == RTLD_NEXT || getLibraryNameFromHandle(handle).find("libSDL2") == 0) {
        OVERRIDE(SDL_Init);
        OVERRIDE(SDL_InitSubSystem);
        OVERRIDE(SDL_QuitSubSystem);
        OVERRIDE(SDL_Quit);
        OVERRIDE(SDL_WasInit);
        OVERRIDE(SDL_JoystickIsHaptic);
        OVERRIDE(SDL_HapticOpen);
        OVERRIDE(SDL_HapticOpened);
        OVERRIDE(SDL_HapticOpenFromJoystick);
        OVERRIDE(SDL_HapticOpenFromMouse);
        OVERRIDE(SDL_HapticClose);
        OVERRIDE(SDL_HapticIndex);
        OVERRIDE(SDL_HapticName);
        OVERRIDE(SDL_HapticNumAxes);
        OVERRIDE(SDL_HapticPause);
        OVERRIDE(SDL_HapticUnpause);
        OVERRIDE(SDL_HapticQuery);
        OVERRIDE(SDL_HapticNewEffect);
        OVERRIDE(SDL_HapticDestroyEffect);
        OVERRIDE(SDL_HapticUpdateEffect);
        OVERRIDE(SDL_HapticEffectSupported);
        OVERRIDE(SDL_HapticGetEffectStatus);
        OVERRIDE(SDL_HapticRunEffect);
        OVERRIDE(SDL_HapticStopEffect);
        OVERRIDE(SDL_HapticStopAll);
        OVERRIDE(SDL_HapticNumEffects);
        OVERRIDE(SDL_HapticNumEffectsPlaying);
        OVERRIDE(SDL_HapticSetGain);
        OVERRIDE(SDL_HapticSetAutocenter);
        OVERRIDE(SDL_HapticRumbleInit);
        OVERRIDE(SDL_HapticRumblePlay);
        OVERRIDE(SDL_HapticRumbleStop);
        OVERRIDE(SDL_HapticRumbleSupported);
    }

#undef OVERRIDE

    return nullptr;
}

extern "C" void *dlsym(void *handle, const char *name)
{
    auto r = dlsym_impl(handle, name);
    if (r != nullptr)
         return r;

    // NOTICE: This call must be optimized to a simple jmp as
    // opposed to call + ret (i.e. we need for a tail call optimization
    // to be applied). This way the real dlsym() will think that
    // it called from the library that called our dlsym(). Otherwise,
    // RTLD_NEXT will work incorrectly.
    // -Os -foptimize-sibling-calls seems to do the trick.
    return real_dlsym(handle, name);
}