/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#pragma once
#include <steam_api.h>

class SteamClientWrapper
{
public:
    SteamClientWrapper();
    ~SteamClientWrapper();
    ISteamController *controller() const { return m_steamController; }
private:
    void disconnect();
private:
    ISteamClient *m_steamClient = nullptr;
    HSteamPipe m_steamPipe = 0;
    HSteamUser m_steamUser = 0;
    ISteamController *m_steamController = nullptr;
};
