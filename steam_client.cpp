/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#include "steam_client.h"
#include "logger.h"

#include <dlfcn.h>
#include <linux/limits.h>
#include <stdarg.h>
#include <stdlib.h>

SteamClientWrapper::SteamClientWrapper()
{
    char path[PATH_MAX];
    snprintf(path, PATH_MAX, "%s/.steam/sdk%i/steamclient.so", getenv("HOME"), ARCH);
    auto steamclient = dlopen(path, RTLD_NOW);
    if (!steamclient) {
        LOG_F(FATAL, "steamclient.so is NOT loaded");
        return;
    }
    LOG_F(INFO, "steamclient.so is loaded");

    typedef ISteamClient *Steamclient_CreateInterface(const char *name, int *return_code);
    auto createInterface = (Steamclient_CreateInterface*)dlsym(steamclient, "CreateInterface");
    if (!createInterface){
        LOG_F(FATAL, "CreateInterface function has NOT been found");
        disconnect();
        return;
    }
    TRACE("CreateInterface function has been found");

    int return_code;
    m_steamClient = createInterface("SteamClient017", &return_code);
    TRACE("client=%p", m_steamClient);
    if (!m_steamClient) {
        LOG_F(FATAL, "SteamClient has NOT been created");
        disconnect();
        return;
    }

    m_steamPipe = m_steamClient->CreateSteamPipe();
    TRACE("steam pipe=%i", m_steamPipe);
    if (!m_steamPipe) {
        LOG_F(FATAL, "SteamPipe has NOT been created");
        disconnect();
        return;
    }

    m_steamUser = m_steamClient->ConnectToGlobalUser(m_steamPipe);
    TRACE("steam user=%i", m_steamUser);
    if (!m_steamUser) {
        LOG_F(FATAL, "Steam user has NOT been connected to");
        disconnect();
        return;
    }

    m_steamController = m_steamClient->GetISteamController(m_steamUser, m_steamPipe, "SteamController006");
    TRACE("steam controller=%p", m_steamController);

    if (!m_steamController->Init()) {
        LOG_F(FATAL, "Steam controller initialization has failed");
        disconnect();
        return;
    }
    LOG_F(INFO, "Steam controller has been initialized!");
}

SteamClientWrapper::~SteamClientWrapper()
{
    disconnect();
}

void SteamClientWrapper::disconnect()
{
    if (m_steamPipe && m_steamClient)
        m_steamClient->BReleaseSteamPipe(m_steamPipe);
    if (m_steamClient)
        m_steamClient->BShutdownIfAllPipesClosed();

    m_steamController = nullptr;
    m_steamUser = 0;
    m_steamPipe = 0;
    m_steamClient = nullptr;
}
