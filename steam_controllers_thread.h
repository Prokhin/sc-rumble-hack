/**
 * Copyright (c) 2018-2019 Alexey Prokhin
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */

#pragma once
#include "steam_controller_haptic.h"
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vector>

struct SteamControllerHapticEffect;
class ISteamController;

class SteamControllersThread 
{
public:
    SteamControllersThread(ISteamController *controller);
    ~SteamControllersThread();
protected:
    friend struct _SDL_Haptic;
    void registerHaptic(SDL_Haptic *haptic);
    void unregisterHaptic(SDL_Haptic *haptic);
    void onHapticEffectStarted(SDL_Haptic *haptic, SteamControllerHapticEffect &effect, Uint32 iterations);
    void onHapticEffectUpdated(SDL_Haptic *haptic, SteamControllerHapticEffect &effect);
    void onHapticEffectStopped(SDL_Haptic *haptic, SteamControllerHapticEffect &effect);
    void onAllHapticEffectsStopped(SDL_Haptic *haptic);
private:
    // Calculates new vibration states of the controllers.
    // Returns true if at least one effect is active and we need the thread to be running
    bool tick();
    void updateVibrationState(SDL_Haptic *haptic, Uint16 leftSpeed, Uint16 rightSpeed);
private:
    std::thread *m_thread = nullptr;
    std::mutex m_mutex;
    std::condition_variable m_cond;
    std::vector<SDL_Haptic*> m_haptics;
    bool m_isStopped;
    ISteamController *m_controller = nullptr;
};
